import unittest
from chiffrement import cryptage, decryptage, cryptage_circulaire, decryptage_circulaire


class TestChiffrementFunctions(unittest.TestCase):
    def test_cryptage(self):
        self.assertEqual(cryptage("CESAR", 8), "KM[IZ")
        self.assertEqual(cryptage("CESAR", 0), "CESAR")

    def test_decryptage(self):
        self.assertEqual(decryptage("CESAR", 8), ";=K9J")
        self.assertEqual(decryptage("CESAR", 0), "CESAR")

    def test_cryptage_circulaire(self):
        self.assertEqual(cryptage_circulaire("CESAR", 8), "KMAIZ")
        self.assertEqual(cryptage_circulaire("cesar", 8), "kmaiz")
        self.assertEqual(cryptage_circulaire("CESAR", 0), "CESAR")

    def test_decryptage_circulaire(self):
        self.assertEqual(decryptage_circulaire("CESAR", 8), "UWKSJ")
        self.assertEqual(decryptage_circulaire("cesar", 8), "uwksj")
        self.assertEqual(decryptage_circulaire("CESAR", 0), "CESAR")