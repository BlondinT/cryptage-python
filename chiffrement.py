from tkinter import *
from tkinter.messagebox import *

NB_DECALAGE = 0

Root = Tk()

for i in range(5):
    Root.rowconfigure(i, weight=1)
for i in range(3):
    Root.columnconfigure(i, weight=1)

lbl_Saisie = Label(Root, text="Saisie")
lbl_Saisie.grid(row=0, column=1, sticky='s')

str_Saisie=StringVar()
txt_Saisie = Entry(Root, textvariable=str_Saisie)
txt_Saisie.grid(row=1, columnspan=3, sticky='n')

int_Saisie=IntVar()
entry_decalage = Entry(Root, textvariable=int_Saisie, width=4)
entry_decalage.grid(row=2, column=1, sticky='n')

bool_circulaire = IntVar()
check_circulaire = Checkbutton(Root, text="cryptage circulaire", variable=bool_circulaire)
check_circulaire.grid(row=3, column=1, sticky='n')

lbl_Resultat = Label(Root, text="Resultat")
lbl_Resultat.grid(row=4, column=1, sticky='n')

txt_Res = Entry(Root)
txt_Res.grid(row=5, columnspan=3, sticky='n')


def cryptage(str_a_modifier, nb_decalage):
    res = ""
    for lettre in str_a_modifier:
        lettre = chr(ord(lettre) + nb_decalage)
        res += lettre
    return res


def boucle_cicrulaire_croissante(lettre, nb_decalage):
    for index in range(0 , nb_decalage):        
        if (lettre >= 'A' and lettre <='Z'):
           lettre = chr(ord(lettre) + 1)
           if lettre > 'Z':
                lettre = 'A' 
        elif (lettre >= 'a' and lettre <='z'):
            lettre = chr(ord(lettre) + 1)
            if lettre > 'z':
                lettre = 'a'  
    return lettre


def boucle_cicrulaire_decroissante(lettre, nb_decalage):
    for index in range(0 , nb_decalage):        
        if (lettre >= 'A' and lettre <='Z'):
           lettre = chr(ord(lettre) - 1)
           if lettre < 'A':
               lettre = 'Z' 
        elif (lettre >= 'a' and lettre <='z'):
            lettre = chr(ord(lettre) - 1)
            if lettre < 'a':
                lettre = 'z'  
    return lettre        
        

def cryptage_circulaire(str_a_modifier, nb_decalage):
    res = ""
    for lettre in str_a_modifier:
            lettre = boucle_cicrulaire_croissante(lettre, nb_decalage)
            res += lettre
    return res


def decryptage(str_a_modifier, nb_decalage):
    res = ""
    for lettre in str_a_modifier:
        lettre = chr(ord(lettre) - nb_decalage)
        res += lettre
    return res


def decryptage_circulaire(str_a_modifier, nb_decalage):
    res = ""
    for lettre in str_a_modifier:
        lettre = boucle_cicrulaire_decroissante(lettre, nb_decalage)
        res += lettre
    return res
    

def command_cryptage(str_Saisie, nb_decalage, bool_circulaire):
    txt_Res.delete(0, END)
    if(bool_circulaire):
        res=cryptage_circulaire(str_Saisie,nb_decalage)
    else:
        res = cryptage(str_Saisie, nb_decalage)
    txt_Res.insert(END, res)


def command_decryptage(str_Saisie, nb_decalage, bool_circulaire):
    txt_Res.delete(0, END)
    if(bool_circulaire):
        res = decryptage_circulaire(str_Saisie, nb_decalage)
    else:
        res = decryptage(str_Saisie,nb_decalage)
    txt_Res.insert(END, res)


button_cryptage = Button(Root, text="Crypter", command=lambda:command_cryptage(str_Saisie.get(), int_Saisie.get(), bool_circulaire.get()))
button_cryptage.grid(row=2, column=0)

button_decryptage = Button(Root, text="Décrypter", command=lambda:command_decryptage(str_Saisie.get(), int_Saisie.get(), bool_circulaire.get()))
button_decryptage.grid(row=2, column=2)

Root.mainloop()